
// Number one question

db.users.find({
	$or: [
			{ firstName: {$regex: 's', $options: '$i'}},
			{ lastName: { $regex: 'd', $options: '$i'}}
		]
}, {firstName: 1, lastName: 1, _id: 0 });


// output

/* 1 */
{
    "firstName" : "Jane",
    "lastName" : "Doe"
}

/* 2 */
{
    "firstName" : "Stephen",
    "lastName" : "Hawking"
}

// Find users who are from the HR department and their age is greater then or equal to 70.

db.users.find({
	$and: [{
		department: "HR"
	},
		{age: {
			$gte: 70
		}
	}
	]
})


// Find users with the letter 'e' in their first name and has an age of less than or equal to 30.

db.users.find({
	$and: [
		{ firstName: {$regex: 'e', $options: '$i'}},
		{ age: {$lte: 30}}
	]
});